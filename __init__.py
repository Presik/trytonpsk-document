# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
import document
import configuration


def register():
    Pool.register(
        configuration.Configuration,
        document.TypeDocument,
        document.DocumentTracking,
        module='document', type_='model')
