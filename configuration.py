# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction


class Configuration(ModelSQL, ModelView):
    'Configuration'
    __name__ = 'document.configuration'
    company_name_path = fields.Char('Company Name Path', required=True)
    company = fields.Many2One('company.company', 'Company')
    path_localhost = fields.Char('Path Localhost')
    path_ftp = fields.Char('Path Ftp')
    path_home = fields.Char('Path Home')

    @classmethod
    def get_configuration(cls):
        company_id = Transaction().context.get('company')
        if company_id:
            config, = cls.search([
                ('company', '=', company_id)
            ])
            return config

    @classmethod
    def __setup__(cls):
        super(Configuration, cls).__setup__()

    @classmethod
    def write(cls, records, values):
        for rec in records:
            if values.get('company_name_path'):
                rec._create_company_name_path(values.get('company_name_path'))
        super(Configuration, cls).write(records, values)

    def _create_company_name_path(self, name):
        path_base = self.path_home
        if path_base is None:
            raise AccessError(gettext('document.msg_missing_path_dms'))
        path_data = os.path.join(path_base, name)
        if not os.path.exists(path_data):
            try:
                os.mkdir(path_data, 0755)
            except Exception as e:
                raise AccessError(gettext('document.msg_invalid_path', s=e))
